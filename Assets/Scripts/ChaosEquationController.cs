﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChaosEquationController : MonoBehaviour
{
    [SerializeField] private int PointsAmount;
    private List<Vector2> PointList = new List<Vector2>();
    private List<GameObject> ScreenPointList = new List<GameObject>();
    [SerializeField] private float tStart;
    [SerializeField] private float TimeScale;
    [SerializeField] private int FPS;
    [SerializeField] private string xExpression;
    [SerializeField] private string yExpression;
    [SerializeField] private Text TimeText;
    [SerializeField] private Text xText;
    [SerializeField] private Text yText;
    [SerializeField] private float Speed;
    [SerializeField] private float tMax;

    private float tCurrent;
    private float xCurrent;
    private float yCurrent;
    private string[] xParts = { "(x*x)", "(x*y)", "(x*t)" };
    private string[] yParts = { "(y*y)", "(x*y)", "(y*t)" };
    private string[] tParts = { "(t*t)","(x*t)", "(y*t)" };
    private string[] simpleParts = { "x", "y", "t" };

    private string[] sign = { "+", "-" };

    void Awake()
    {
        Application.targetFrameRate = FPS;
    }
    void Start()
    {
        InitializeTime();
        xExpression = xExpression == "" ? InitializeFunctionString() : xExpression;
        yExpression = yExpression == "" ? InitializeFunctionString() : yExpression;
        xText.text = "X' = " + xExpression;
        yText.text = "Y' = " + yExpression;
        InitializePointPosition();
        InitializePointsOnScreen();
    }

    // Update is called once per frame
    void Update()
    {
        Time.timeScale = TimeScale;
        UpdateTime();
        UpdatePointsPosition();
        UpdatePointsOnScreen();
        Inputs();
    }

    private string InitializeFunctionString()
    {
        string funcString = string.Empty;
        
        int rX = Random.Range(1,3);
        int rY = Random.Range(1,3);
        int rT = Random.Range(1,3);

        int rS = Random.Range(0,2);

        for(int i = 0; i < rX; i++) {
            funcString += sign[Random.Range(0, sign.Length)];
            funcString += xParts[Random.Range(0, xParts.Length)];
        }

        for(int i = 0; i < rY; i++) {
            funcString += sign[Random.Range(0, sign.Length)];
            funcString += yParts[Random.Range(0, yParts.Length)];
        }

        for(int i = 0; i < rT; i++) {
            funcString += sign[Random.Range(0, sign.Length)];
            funcString += tParts[Random.Range(0, tParts.Length)];
        }

        for(int i = 0; i < rS; i++) {
            funcString += sign[Random.Range(0, sign.Length)];
            funcString += simpleParts[Random.Range(0, simpleParts.Length)];
        }

        return funcString == string.Empty ? "t" : funcString;
    }
    private void UpdatePointsPosition()
    {
        xCurrent = tCurrent;
        yCurrent = tCurrent;

        for (int i = 0; i < PointList.Count; i++)
        {
            Vector2 newPoint = CoordinatesFromExpression(xExpression, yExpression, tCurrent, xCurrent, yCurrent, i);
            PointList[i] = newPoint;
            xCurrent = PointList[i].x;
            yCurrent = PointList[i].y;
        }
    }

    private void Inputs() {
        if(Input.GetKey(KeyCode.F5)) {
            SceneManager.LoadScene("SampleScene");
        }
    }

    private void InitializePointPosition()
    {
        for (int i = 0; i < PointsAmount; i++)
        {
            Vector2 coordinatesFromExpression = CoordinatesFromExpression(xExpression, yExpression, tCurrent, xCurrent, yCurrent, i);
            PointList.Add(coordinatesFromExpression);
            xCurrent = PointList[PointList.Count - 1].x;
            yCurrent = PointList[PointList.Count - 1].y;
        }
    }

    private Vector2 CoordinatesFromExpression(string xExp, string yExp, float t, float x, float y, int i)
    {
        Vector2 result = Vector2.zero;

        xExp = ReplaceExpressionVariables(xExp, t, x, y);
        yExp = ReplaceExpressionVariables(yExp, t, x, y);

        DataTable dt = new DataTable();

        try
        {
            result.x = float.Parse(dt.Compute(xExp.Replace(',', '.'), "").ToString());
            result.y = float.Parse(dt.Compute(yExp.Replace(',', '.'), "").ToString());
        }
        catch (System.OverflowException e)
        {
            try
            {
                return PointList[i];
            }
            catch (System.ArgumentOutOfRangeException a)
            {
                Debug.Log("peepee hard :(");
                SceneManager.LoadScene("SampleScene");
            }
        }
        return result;
    }

    private void InitializeTime()
    {
        tCurrent = tStart;
    }

    private void UpdateTime()
    {
        tCurrent += Time.deltaTime;
        TimeText.text = "t = " + tCurrent;

        if (tCurrent > tMax)
        {
            SceneManager.LoadScene("SampleScene");
        }
    }

    private string ReplaceExpressionVariables(string expression, float t, float x, float y)
    {
        expression = expression.Replace("x", x.ToString());
        expression = expression.Replace("X", x.ToString());
        expression = expression.Replace("y", y.ToString());
        expression = expression.Replace("Y", y.ToString());
        expression = expression.Replace("t", t.ToString());
        expression = expression.Replace("T", t.ToString());
        return expression;
    }

    private void InitializePointsOnScreen()
    {
        foreach (Vector2 Point in PointList)
        {
            GameObject ScreenPoint = GameObject.Instantiate(Resources.Load("Prefabs/Point"), new Vector3(Point.x, Point.y, 0), Quaternion.identity) as GameObject;
            Color32 col = new Color32((byte)Random.Range(100, 255f), (byte)Random.Range(100, 255f), (byte)Random.Range(100, 255f), 255);
            ScreenPoint.GetComponent<SpriteRenderer>().color = col;
            ParticleSystem.MainModule main = ScreenPoint.GetComponentInChildren<ParticleSystem>().main;
            main.startColor = new ParticleSystem.MinMaxGradient(col);
            ScreenPointList.Add(ScreenPoint);
        }
    }

    private void UpdatePointsOnScreen()
    {
        for (int i = 0; i < ScreenPointList.Count; i++)
        {
            ScreenPointList[i].transform.position = new Vector3(PointList[i].x, PointList[i].y, 0);
        }
    }

    public void NewGen()
    {
        SceneManager.LoadScene("SampleScene");
    }

}

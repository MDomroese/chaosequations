﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point : MonoBehaviour
{
    void Start() {
        GetComponentInChildren<GameObject>().SetActive(false);
    }
    void OnBecameVisible() {
        GetComponentInChildren<GameObject>().SetActive(true);
    }

    void OnBecameInvisible() {
        GetComponentInChildren<GameObject>().SetActive(false);
    }
}
